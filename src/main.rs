use std::path;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::process::Command;

#[macro_use] extern crate log;
extern crate env_logger;

#[macro_use] extern crate serde_derive;
extern crate docopt;
use docopt::Docopt;

extern crate toml;

extern crate regex;
use regex::bytes::Regex;

#[macro_use] extern crate tera;
use std::collections::HashMap;
use tera::{Result, Value, to_value};

extern crate rayon;
use rayon::prelude::*;

fn recursive_find(p : &path::Path, v : &mut Vec<path::PathBuf>, re : &Regex) {
    if p.is_dir() {
        match fs::read_dir(p) {
            Err(why) => warn!("{:?}", why),
            Ok(entries) => for entry in entries {
                match entry {
                    Err(why) => warn!("{:?}", why),
                    Ok(entry) => recursive_find(entry.path().as_path(), v, re)
                }
            }
        }
    } else {
        match p.to_str() {
            None => warn!("Cannot handle: {}", p.display()),
            Some(s) => if re.is_match(s.as_bytes()) {
                v.push(p.to_path_buf());
            }
        }
    }
}

fn read_dir(p : &path::Path, re : &Regex) -> Vec<path::PathBuf> {
    let mut files : Vec<path::PathBuf> = Vec::new();
    recursive_find(&p, &mut files, re);
    files
}

static USAGE: &'static str = "
Usage: rpexec <cfg> <folder>
       rpexec --help

Options:
    -h, --help     Show this help.
";

#[derive(Debug, Deserialize)]
struct Args {
    arg_cfg: Option<String>,
    arg_folder: Option<String>,
    flag_help: bool,
}

#[derive(Deserialize)]
struct Config {
    regex: String,
    cmds: Vec<String>,
}

pub fn basename(value: Value, _: HashMap<String, Value>) -> Result<Value> {
    let s = try_get_value!("upper", "value", String, value);
    let p = path::Path::new(&s);
    let basename = p.file_stem().unwrap().to_str().unwrap();
    Ok(to_value(basename).unwrap())
}

pub fn parent(value: Value, _: HashMap<String, Value>) -> Result<Value> {
    let s = try_get_value!("upper", "value", String, value);
    let p = path::Path::new(&s);
    let basename = p.parent().unwrap().to_str().unwrap();
    Ok(to_value(basename).unwrap())
}

pub fn run_command(cmdline : String) {
    // println!("\nexecuting: {}", cmdline);
    let output;
    if cfg!(target_os = "windows") {
        output = Command::new("cmd.exe").args(&["/C", &cmdline]).output().unwrap();
    } else {
        output = Command::new("sh").args(&["-c", &cmdline]).output().unwrap();
    }
    let stdout = String::from_utf8_lossy(&output.stdout);
    if stdout.len() > 0 {
        print!("{}", stdout);
    }
    let stderr = String::from_utf8_lossy(&output.stderr);
    if stderr.len() > 0 {
        eprint!("{}", stderr);
    }
}

pub fn handle_path(p : &path::PathBuf, cmds : &Vec<String>) {
    match p.to_str() {
        None => {
            warn!("Cannot handle: {}", p.display());
        },
        Some(s) =>  {
            println!("Processing {}", &s);
            for cmd in cmds {
                let mut context = tera::Context::new();
                context.add("path", &s);
                let mut tmpl = tera::Tera::default();
                tmpl.register_filter("basename", basename);
                tmpl.register_filter("parent", parent);
                tmpl.add_raw_template("one_off", &cmd).expect("failed");
                match tmpl.render("one_off", &context) {
                    Err(why) => warn!("{:?}", why),
                    Ok(result) => run_command(result)
                }
            }
        }
    }
}

fn main() {
    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.deserialize())
                            .unwrap_or_else(|e| e.exit());
    println!("{:?}", args);
    let cfg = args.arg_cfg.unwrap();
    let mut f = File::open(cfg).expect("File not found");
    let mut content = String::new();
    f.read_to_string(&mut content).expect("Reading failed");
    println!("Content: {}", content);
    let config : Config = toml::from_str(&content).unwrap();
    println!("toml: regex = {}", config.regex);
    let re = Regex::new(&config.regex).unwrap();
    let folder = args.arg_folder.unwrap();
    match env_logger::init() {
        Err(why) => panic!("{:?}", why),
        Ok(_) => ()
    }
    let p = path::Path::new(&folder);
    let files = read_dir(&p, &re);
    // files.iter().for_each(|p| handle_path(p, &config.cmds));
    files.par_iter().for_each(|p| handle_path(p, &config.cmds));
}

